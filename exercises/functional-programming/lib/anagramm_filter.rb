# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, anagramms)
  anagramms.select { |s| s.split('').sort == word.split('').sort }
end
# END
