# frozen_string_literal: true

# BEGIN
def get_same_parity(arr)
  return arr if arr.empty?
  return arr.select(&:even?) if arr[0].even?
  return arr.reject(&:even?) unless arr[0].even?
end
# END
