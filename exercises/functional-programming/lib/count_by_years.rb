# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users.each_with_object({}) do |user, hash|
    next unless user[:gender] == 'male'

    year = user[:birthday].split('-').first
    hash[year].nil? ? hash[year] = 1 : hash[year] += 1
  end
end
# END
