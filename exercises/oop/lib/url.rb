# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

# Url class
class Url
  extend Forwardable
  include Comparable
  include URI

  attr_reader :url, :query_params

  def_delegators :@url, :scheme, :host

  def initialize(url)
    @url = URI(url)
    parse_params
  end

  def query_param(key, value = nil)
    query_params[key] || value
  end

  def <=>(other)
    url <=> other.url
  end

  private

  def parse_params
    params = url.query
    return unless params

    @query_params = params.split('&').map do |param|
      key, value = param.split('=')
      [key.to_sym, value]
    end.to_h
  end
end
# END
