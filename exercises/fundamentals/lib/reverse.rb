# frozen_string_literal: true

# BEGIN
def reverse(string)
  new_string = []
  string.each_char do |char|
    new_string.unshift(char)
  end

  new_string.join
end
# END
