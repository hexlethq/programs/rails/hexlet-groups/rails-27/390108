# frozen_string_literal: true

# BEGIN
def get_string_value(num)
  return 'FizzBuzz' if (num % 3).zero? && (num % 5).zero?
  return 'Fizz' if (num % 3).zero?
  return 'Buzz' if (num % 5).zero?

  num.to_s
end

def fizz_buzz(start, stop)
  return if start > stop

  array = []
  (start..stop).each do |a|
    array << get_string_value(a)
  end
  array.join(' ')
end
# END
