# frozen_string_literal: true

# BEGIN
def fibonacci(fib)
  return nil if fib <= 0
  return 0 if fib == 1
  return 1 if fib == 2

  fibonacci(fib - 1) + fibonacci(fib - 2)
end
# END
