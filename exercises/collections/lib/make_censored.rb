# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  words = text.split(' ')
  words.map! do |word|
    word = '$#%!' if stop_words.include?(word)
    word
  end
  words.join(' ')
  # END
end
