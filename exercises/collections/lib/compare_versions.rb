# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  ar1 = version1.split('.').map(&:to_i)
  ar2 = version2.split('.').map(&:to_i)
  ar1 <=> ar2
end
# END
