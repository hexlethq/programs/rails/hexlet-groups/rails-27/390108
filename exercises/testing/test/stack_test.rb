# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new ['ruby', 'php', 'java']
  end

  def test_empty_stack
    @stack = Stack.new
    assert_empty @stack
    assert_equal @stack.to_a, []
    assert_equal @stack.size, 0
  end

  def test_stack_push
    @stack.push! 'python'
    assert_equal @stack.to_a, ['ruby', 'php', 'java', 'python']
    assert_equal @stack.size, 4
    refute_empty @stack
  end

  def test_pop
    @stack.pop!
    assert_equal @stack.to_a, ['ruby', 'php']
  end

  def test_pop_on_empty_stack
    @stack = Stack.new
    @stack.pop!
    assert_empty @stack
  end

  def test_clear
   @stack.clear!
   assert_empty @stack
   assert_equal @stack.to_a, []
  end 
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
