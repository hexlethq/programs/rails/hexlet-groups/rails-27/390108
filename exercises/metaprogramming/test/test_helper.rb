# frozen_string_literal: true

require 'bundler/setup'
Bundler.require

require 'minitest/autorun'
require 'minitest/pride'
