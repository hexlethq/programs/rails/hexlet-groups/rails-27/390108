# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(attributes = {})
    @attributes = self.class.schema.keys.map { |key| [key, convert(key, attributes[key])] }.to_h
  end

  def attributes
    @attributes
  end

  # Methods
  module ClassMethods
    attr_reader :schema

    def attribute(name, options = {})
      @schema ||= {}
      @schema[name] = options

      define_method(name) do
        @attributes[name]
      end

      define_method("#{name}=") do |val|
        @attributes[name] = convert(name, val)
      end
    end
  end

  private

  def convert(attribute, value)
    return if value.nil?

    case self.class.schema[attribute][:type]
    when :integer
      value.to_i
    when :string
      value.to_s
    when :datetime
      DateTime.parse(value)
    when :boolean
      !!value
    end
  end
end
# END
